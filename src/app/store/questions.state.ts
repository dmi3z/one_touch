import { QuestionsData } from './../types/questions.interface';
export interface QuestionsState {
    personalStep: number;
    mainStep: number;
    questions: QuestionsData;
    personalAnswers: number[];
    mainAnswers: Array<{id: number, value: number}>;
    isMainStepsActive: boolean;
}

export const initialQuestionsState: QuestionsState = {
    personalStep: 0,
    mainStep: 0,
    questions: {
        personalInfo: [],
        questions: []
    },
    personalAnswers: [],
    mainAnswers: [],
    isMainStepsActive: false
};

