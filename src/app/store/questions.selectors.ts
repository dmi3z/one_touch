export const selectPersonalStep = (state) => state.questions.personalStep;
export const selectMainStep = (state) => state.questions.mainStep;
export const selectCurrentPersonalQuestion = (state) => state.questions.questions.personalInfo[state.questions.personalStep];
export const selectCurrentMainQuestion = (state) => state.questions.questions.questions[state.questions.mainStep];
export const selectIsMainStepsActive = (state) => state.questions.isMainStepsActive;
export const selectCurrentPersonalAnswer = (state) => state.questions.personalAnswers[state.questions.personalStep];
export const selectCurrentMainAnswer = (state) => state.questions.mainAnswers[state.questions.mainStep];
export const selectIsJump = (state) =>
    state.questions.personalAnswers.length === state.questions.questions.personalInfo.length &&
    state.questions.mainStep === 0 && !state.questions.isMainStepsActive &&
    state.questions.personalStep === state.questions.personalAnswers.length - 1;

export const selectIsStart = (state) => state.questions.personalStep === 0;
export const selectIsEnd = (state) => state.questions.mainStep === state.questions.questions.questions.length - 1;
export const selectMainAnswers = (state) => state.questions.mainAnswers;
export const selectPersonalAnswers = (state) => state.questions.personalAnswers;
export const selectQuestionsData = (state) => state.questions.questions;
