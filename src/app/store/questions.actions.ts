import { QuestionsData } from './../types/questions.interface';
import { createAction, props } from '@ngrx/store';

export enum EQuestionActions {
    GET_PERSONAL_STEP = '[STEP] Get personal step',
    // SET_PERSONAL_STEP = '[STEP] Set personal step',
    // GET_PERSONAL_ANSWERS = '[ANSWERS] Get personal answers',
    // SET_PERSONAL_ANSWER = '[ANSWERS] Set personal answer',
    GET_MAIN_STEP = '[STEP] Get main step',
    // GET_IS_MAIN_STEPS = '[STEP] Get is main steps active'
    INCREMENT_STEP = '[STEP] Incrment step',
    DECREMENT_STEP = '[STEP] Decrement step',
    LOAD_QUESTIONS = '[QUESTIONS] Load questions',
    SET_ANSWER = '[ANSWER] Set answer'
}

export const incrementStep = createAction(EQuestionActions.INCREMENT_STEP);
export const decrementStep = createAction(EQuestionActions.DECREMENT_STEP);
export const loadQuestions = createAction(EQuestionActions.LOAD_QUESTIONS, props<QuestionsData>());
export const setAnswer = createAction(EQuestionActions.SET_ANSWER, props<{ answer: number }>());

