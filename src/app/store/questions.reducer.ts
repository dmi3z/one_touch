import { Action, ActionReducerMap } from '@ngrx/store';
import { createReducer, on } from '@ngrx/store';
import { decrementStep, incrementStep, loadQuestions, setAnswer } from './questions.actions';
import { initialQuestionsState, QuestionsState } from './questions.state';

export interface AppState {
    questions: QuestionsState;
}

const questionsReducer = createReducer(
    initialQuestionsState,
    on(incrementStep, state => {
        if (state.isMainStepsActive) {
            if (state.mainAnswers[state.mainStep] !== undefined) {
                if (state.mainStep < state.questions.questions.length - 1) {
                    return { ...state, mainStep: state.mainStep + 1 };
                }
                return { ...state };
            }
            return { ...state };
        } else {
            if (state.personalAnswers[state.personalStep] !== undefined) {
                if (state.personalStep < state.questions.personalInfo.length - 1) {
                    return { ...state, personalStep: state.personalStep + 1 };
                }
                return { ...state, isMainStepsActive: true };
            }
            return { ...state };
        }
    }),
    on(decrementStep, state => {
        if (state.isMainStepsActive) {
            if (state.mainStep > 0) {
                return { ...state, mainStep: state.mainStep - 1 };
            }
            return { ...state, isMainStepsActive: false };
        } else {
            if (state.personalStep > 0) {
                return { ...state, personalStep: state.personalStep - 1 };
            }
            return { ...state };
        }
    }),
    on(loadQuestions, (state, data) => ({
        ...state,
        questions: { ...data }
    })),
    on(setAnswer, (state, { answer }) => {
        if (state.isMainStepsActive) {
            const answers = [...state.mainAnswers];
            const data = {
                id: state.mainStep,
                value: answer
            };
            answers[state.mainStep] = data;
            return {
                ...state,
                mainAnswers: [...answers]
            };
        } else {
            const answers = [...state.personalAnswers];
            answers[state.personalStep] = answer;
            return {
                ...state,
                personalAnswers: [...answers]
            };
        }
    })
);

export function reducer(state: QuestionsState | undefined, action: Action) {
    return questionsReducer(state, action);
}

export const reducers: ActionReducerMap<AppState> = {
    questions: reducer
};
