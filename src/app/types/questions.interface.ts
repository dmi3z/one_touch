export interface QuestionsData {
    personalInfo: PersonalQuestion[];
    questions: Array<string[]>;
}

export interface PersonalQuestion {
    title: string;
    values: string[];
}

export interface MainAnswer {
    id: number;
    value: number;
}
