export interface ResultsDTO {
    data: ResultsData;
    status: number;
}

export interface ResultsData {
    person: string;          // name of person
    psychoTypeDesc: string; // person description
    fullProfileData: Array<string[]>; // key - value невротизм, стиль мышления
    portraitDesc: Array<string[]>; // тип реагирования, поведенческие признаки
    portraitDescSoft: Array<string[]>; // свойства характера
    personPicture: string[];
}
