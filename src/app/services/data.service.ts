import { ResultsDTO } from './../types/results.interface';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MainAnswer, QuestionsData } from '../types/questions.interface';

@Injectable({ providedIn: 'root' })

export class DataService {
    private readonly BASE_URL = ' https://salary.nobugs.today/api/v1/test/';

    constructor(private http: HttpClient) { }

    public getQuestions(lang = 'ru'): Observable<QuestionsData> {
        const params = new HttpParams().set('lang', lang);
        return this.http.get<QuestionsData>(this.BASE_URL.concat('questions'), { params });
    }

    public getPersonResult(encodedData: string, lang = 'ru'): Observable<ResultsDTO> {
        const params = new HttpParams().append('lang', lang).append('encdata', encodedData);
        return this.http.get<ResultsDTO>(this.BASE_URL.concat('person'), { params });
    }

    public prepareMainAnswers(mainAnswers: MainAnswer[]): Array<number[]> {
        const arrSum = [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]];

        arrSum.forEach((item, i) => {
            let k = i;
            item.forEach((value, j) => {
                [0, 0, 0].forEach(() => {
                    arrSum[i][j] += +mainAnswers[k].value;
                    k += 5;
                });
            });
        });
        return [...arrSum];
    }

    public getAllAnswersPrepared(personalAnswers: number[], mainAnswers: Array<number[]>): Array<number[] | Array<number[]>> {
        return [personalAnswers, mainAnswers];
    }

    public getBase64Data(data: Array<number[] | Array<number[]>>): string {
        const stringifyData = JSON.stringify(data);
        const base64string = btoa(stringifyData);
        return base64string;
    }
}
