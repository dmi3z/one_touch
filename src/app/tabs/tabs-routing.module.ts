import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'test-tab',
        loadChildren: () => import('./pages/test-tab/test-tab.module').then(m => m.TestTabPageModule)
      },
      {
        path: 'celebrities-tab',
        loadChildren: () => import('./pages/celebrities-tab/celebrities-tab.module').then(m => m.CelebritiesTabPageModule)
      },
      {
        path: 'friends-tab',
        loadChildren: () => import('./pages/friends-tab/friends-tab.module').then(m => m.FriendsTabPageModule)
      },
      {
        path: 'compare',
        loadChildren: () => import('./pages/compare/compare.module').then(m => m.ComparePageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/test-tab',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/test-tab/start',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
