export enum NavigationStep {
    PREVIOUS_STEP = 'previous',
    NEXT_STEP = 'next'
}
