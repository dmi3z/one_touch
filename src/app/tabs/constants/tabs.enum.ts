export enum Tabs {
    TEST_TAB = 'test-tab',
    CELEBRITIES_TAB = 'celebrities-tab',
    FRIENDS_TAB = 'friends-tab'
}
