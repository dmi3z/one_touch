import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StartTestPageComponent } from './pages/start-test/start-test.page';
import { TestTabPage } from './test-tab.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/tabs/test-tab/start',
    pathMatch: 'full'
  },
  {
    path: '',
    component: TestTabPage,
    children: [
      {
        path: 'start',
        component: StartTestPageComponent
      },
      {
        path: 'intro',
        loadChildren: () => import('./pages/intro/intro.module').then(m => m.IntroModule)
      },
      {
        path: 'test-view',
        loadChildren: () => import('./pages/test-view/test-view.module').then(m => m.TestViewPageModule)
      },
      {
        path: 'next-step',
        loadChildren: () => import('./pages/next-step/nex-step.module').then(m => m.NextStepPageModule)
      },
      {
        path: 'results',
        loadChildren: () => import('./pages/results/results.module').then(m => m.ResultsPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestTabPageRoutingModule {}
