import { NavigationsModule } from '../../../../components/navigations/navigations.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { NextStepPageComponent } from './next-step.page';

@NgModule({
    declarations: [
        NextStepPageComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        NavigationsModule,
        RouterModule.forChild([
            {
                path: '',
                component: NextStepPageComponent
            }
        ])
    ]
})

export class NextStepPageModule { }
