import { DataService } from 'src/app/services/data.service';
import { NavigationStep } from '../../../../constants/navigation.enum';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/questions.reducer';
import { decrementStep, incrementStep } from 'src/app/store/questions.actions';

@Component({
    selector: 'app-next-step',
    templateUrl: 'next-step.page.html',
    styleUrls: ['next-step.page.scss']
})

export class NextStepPageComponent {

    constructor(private router: Router, private store: Store<AppState>) {}

    public onNavigationChange(stepName: NavigationStep): void {
        if (stepName === NavigationStep.PREVIOUS_STEP) {
            this.router.navigate(['/tabs/test-tab/test-view']);
        }
        if (stepName === NavigationStep.NEXT_STEP) {
            this.store.dispatch(incrementStep());
            this.router.navigate(['/tabs/test-tab/test-view']);
        }
    }
}
