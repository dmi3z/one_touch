import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-start-test-page',
    templateUrl: './start-test.page.html',
    styleUrls: ['start-test.page.scss']
})

export class StartTestPageComponent {

    constructor(private router: Router) {}

    public openIntro(): void {
        this.router.navigate(['/tabs/test-tab/results']);
    }

}
