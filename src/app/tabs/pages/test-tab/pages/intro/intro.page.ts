import { NavigationStep } from '../../../../constants/navigation.enum';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-intro-page',
    templateUrl: 'intro.page.html',
    styleUrls: ['intro.page.scss']
})

export class IntroPageComponent {

    constructor(private router: Router) {}

    public onNavigationChange(stepName: NavigationStep): void {
        if (stepName === NavigationStep.PREVIOUS_STEP) {
            this.router.navigate(['/tabs/test-tab/start']);
        }
        if (stepName === NavigationStep.NEXT_STEP) {
            this.router.navigate(['/tabs/test-tab/test-view']);
        }
    }
}
