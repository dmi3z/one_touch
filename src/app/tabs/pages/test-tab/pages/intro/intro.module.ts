import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { IntroPageComponent } from './intro.page';
import { NgModule } from '@angular/core';
import { NavigationsModule } from '../../../../components/navigations/navigations.module';

@NgModule({
    declarations: [IntroPageComponent],
    imports: [IonicModule, CommonModule, NavigationsModule, RouterModule.forChild([
        {
            path: '',
            component: IntroPageComponent
        }
    ])],
    exports: [IntroPageComponent]
})

export class IntroModule { }
