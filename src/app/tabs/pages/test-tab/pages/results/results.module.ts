import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { ResultsPageComponenet } from './results.page';
import { ProfileDescriptionComponent } from '../../../../components/profile-description/profile-description.component';
import { ProfileDescriptionModule } from 'src/app/tabs/components/profile-description/profile-description.module';

@NgModule({
    declarations: [
        ResultsPageComponenet
    ],
    imports: [
        IonicModule,
        CommonModule,
        ProfileDescriptionModule,
        RouterModule.forChild([
            {
                path: '',
                component: ResultsPageComponenet
            }
        ])
    ]
})

export class ResultsPageModule { }
