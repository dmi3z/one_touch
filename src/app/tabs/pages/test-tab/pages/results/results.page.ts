import { Observable } from 'rxjs';
import { ResultsDTO } from '../../../../../types/results.interface';
import { take } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-results-page',
    templateUrl: 'results.page.html',
    styleUrls: ['results.page.scss']
})

export class ResultsPageComponenet implements OnInit {

    public testResults$: Observable<ResultsDTO>;

    constructor(private activatedRoute: ActivatedRoute, private dataService: DataService) { }

    public ngOnInit(): void {
        const temp = 'W1sxLDEsMF0sW1stMSwtNiwtMSwwLC0zXSxbLTQsLTIsLTEsLTIsLTFdLFsxLC01LC0xLDEsMV0sWzAsMCwtMSwtNSwtMl0sWzAsMCwyLDEsLTRdXV0=';
        const base64string = this.activatedRoute.snapshot.queryParams.data;
        // if (base64string) {
        this.testResults$ = this.dataService.getPersonResult(temp);
        // }
    }
}
