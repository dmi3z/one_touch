import { NavigationsModule } from '../../../../components/navigations/navigations.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TestViewPageComponent } from './test-view.page';
import { PersonalQuestionComponent } from '../../../../components/personal-question/personal-question.component';
import { MainQuestionComponent } from '../../../../components/main-question/main-question.component';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        TestViewPageComponent,
        PersonalQuestionComponent,
        MainQuestionComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        NavigationsModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: TestViewPageComponent
            }
        ])
    ],
    exports: [
        TestViewPageComponent
    ]
})

export class TestViewPageModule {}
