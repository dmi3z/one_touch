import { selectCurrentPersonalQuestion, selectCurrentMainQuestion, selectIsMainStepsActive, selectCurrentPersonalAnswer, selectCurrentMainAnswer, selectIsJump, selectIsStart, selectIsEnd, selectMainAnswers, selectPersonalAnswers, selectMainStep, selectQuestionsData } from '../../../../../store/questions.selectors';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';
import { decrementStep, incrementStep, loadQuestions, setAnswer } from 'src/app/store/questions.actions';

import { QuestionsState } from 'src/app/store/questions.state';
import { MainAnswer, PersonalQuestion, QuestionsData } from 'src/app/types/questions.interface';
import { NavigationStep } from '../../../../constants/navigation.enum';

@Component({
    selector: 'app-test-view',
    templateUrl: 'test-view.page.html',
    styleUrls: ['test-view.page.scss']
})

export class TestViewPageComponent implements OnInit {

    public questionsData: Observable<QuestionsData>;
    public currentPersonalQuestion: Observable<PersonalQuestion>;
    public currentMainQuestion: Observable<string[]>;
    public isMainStepsActive: Observable<boolean>;
    public currentPersonalAnswer: Observable<number>;
    public currentMainAnswer: Observable<MainAnswer>;
    public mainStep: Observable<number>;

    private mainAnswers: Observable<MainAnswer[]>;
    private personalAnswers: Observable<number[]>;
    private isJump: Observable<boolean>;
    private isStart: Observable<boolean>;
    private isEnd: Observable<boolean>;

    constructor(private dataService: DataService, private router: Router, private store: Store<QuestionsState>) { }

    public ngOnInit(): void {
        this.dataService.getQuestions().pipe(take(1)).subscribe(res => this.store.dispatch(loadQuestions(res)));
        this.initializeStreams();
    }

    public onStepChange(stepName: NavigationStep): void {
        let isJump: boolean;
        let isStart: boolean;
        let isEnd: boolean;

        this.isJump.pipe(take(1)).subscribe(res => isJump = res);
        this.isStart.pipe(take(1)).subscribe(res => isStart = res);
        this.isEnd.pipe(take(1)).subscribe(res => isEnd = res);

        if (stepName === NavigationStep.NEXT_STEP) {
            if (isJump) {
                this.router.navigate(['tabs/test-tab/next-step']);
            } else {
                if (isEnd) {
                    this.openResults();
                } else {
                    this.store.dispatch(incrementStep());
                }
            }
        }
        if (stepName === NavigationStep.PREVIOUS_STEP) {
            this.store.dispatch(decrementStep());
            if (isStart) {
                this.router.navigate(['tabs/test-tab/intro']);
            }
        }
    }

    public onPersonalQuestionChange(value: number): void {
        this.store.dispatch(setAnswer({ answer: value }));
    }

    public onMainQuestionChange(value: number): void {
        this.store.dispatch(setAnswer({ answer: value }));
    }

    private initializeStreams(): void {
        this.questionsData = this.store.pipe(select(selectQuestionsData));
        this.currentPersonalQuestion = this.store.pipe(select(selectCurrentPersonalQuestion));
        this.currentMainQuestion = this.store.pipe(select(selectCurrentMainQuestion));
        this.isMainStepsActive = this.store.pipe(select(selectIsMainStepsActive));
        this.currentPersonalAnswer = this.store.pipe(select(selectCurrentPersonalAnswer));
        this.currentMainAnswer = this.store.pipe(select(selectCurrentMainAnswer));
        this.isJump = this.store.pipe(select(selectIsJump));
        this.isStart = this.store.pipe(select(selectIsStart));
        this.isEnd = this.store.pipe(select(selectIsEnd));
        this.mainAnswers = this.store.pipe(select(selectMainAnswers));
        this.personalAnswers = this.store.pipe(select(selectPersonalAnswers));
        this.mainStep = this.store.pipe(select(selectMainStep));
    }

    private openResults(): void {
        let mainAnswers: MainAnswer[];
        let personalAnswers: number[];
        this.mainAnswers.pipe(take(1)).subscribe(res => mainAnswers = res);
        this.personalAnswers.pipe(take(1)).subscribe(res => personalAnswers = res);
        const preparedMainAnswers = this.dataService.prepareMainAnswers(mainAnswers);
        const preparedAllAnswers = this.dataService.getAllAnswersPrepared(personalAnswers, preparedMainAnswers);
        const base64string = this.dataService.getBase64Data(preparedAllAnswers);

        this.router.navigate(['tabs/test-tab/results'], { queryParams: { data: base64string } });
    }

}
