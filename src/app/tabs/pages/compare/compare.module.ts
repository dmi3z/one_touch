import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from "@angular/core";
import { ComparePageCompoenent } from "./compare.page";
import { ProfileDescriptionModule } from '../../components/profile-description/profile-description.module';
import { NgCircleProgressModule } from 'ng-circle-progress';

@NgModule({
    declarations: [
        ComparePageCompoenent
    ],
    imports: [
        CommonModule,
        IonicModule,
        ProfileDescriptionModule,
        NgCircleProgressModule.forRoot({
            radius: 100,
            outerStrokeWidth: 2,
            innerStrokeWidth: 2,
            outerStrokeColor: "#78C000",
            innerStrokeColor: "#C7E596",
            animationDuration: 300,
          }),
        RouterModule.forChild([
            {
                path: '',
                component: ComparePageCompoenent
            }
        ])
    ]
})

export class ComparePageModule { }
