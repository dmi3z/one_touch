import { NavigationsModule } from './../../../../components/navigations/navigations.module';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { ActionsPageComponent } from './actions-page.component';
import { NgModule } from "@angular/core";

@NgModule({
    declarations: [
        ActionsPageComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        NavigationsModule,
        RouterModule.forChild([
            {
                path: '',
                component: ActionsPageComponent
            }
        ])
    ]
})

export class ActionsPageModule { }
