import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AddFriendsPageComponent } from './add-friends.component';

@NgModule({
    declarations: [
        AddFriendsPageComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: AddFriendsPageComponent
            }
        ])
    ]
})

export class AddFriendsPageModule { }
