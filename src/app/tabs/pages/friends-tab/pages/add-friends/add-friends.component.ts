import { Router } from '@angular/router';
import { Component } from '@angular/core';

@Component({
    selector: 'app-add-friends',
    templateUrl: 'add-friends.component.html',
    styleUrls: ['add-friends.component.scss']
})

export class AddFriendsPageComponent {

    constructor(private router: Router) { }

    public openActions(): void {
        this.router.navigate(['tabs/friends-tab/actions'])
    }
}
