import { AddFriendsPageComponent } from './pages/add-friends/add-friends.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FriendsTabPage } from './friends-tab.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/tabs/friends-tab/add-friends',
    pathMatch: 'full'
  },
  {
    path: '',
    component: FriendsTabPage,
    children: [
      {
        path: 'add-friends',
        component: AddFriendsPageComponent
      },
      {
        path: 'actions',
        loadChildren: () => import('./pages/actions-page/actions-page.module').then(m => m.ActionsPageModule)
      }
    ]
  },
  // {
  //   path: '',
  //   redirectTo: '/tabs/friends-tab/add-friends',
  //   pathMatch: 'full'
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FriendsTabPageRoutingModule {}
