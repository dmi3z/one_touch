import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CelebritiesTabPage } from './celebrities-tab.page';

import { CelebritiesTabPageRoutingModule } from './celebrities-tab-routing.module';
import { GenderFilterModule } from '../../components/gender-filter/gender-filter.module';
import { UserCardModule } from '../../components/user-card/user-card.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    CelebritiesTabPageRoutingModule,
    GenderFilterModule,
    UserCardModule
  ],
  declarations: [CelebritiesTabPage]
})
export class CelebritiesTabPageModule {}
