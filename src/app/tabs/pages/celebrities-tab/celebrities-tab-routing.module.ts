import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CelebritiesTabPage } from './celebrities-tab.page';

const routes: Routes = [
  {
    path: '',
    component: CelebritiesTabPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CelebritiesTabPageRoutingModule {}
