import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PromoModalComponent } from '../../components/promo-modal/promo-modal.component';

@Component({
  selector: 'app-celebrities',
  templateUrl: 'celebrities-tab.page.html',
  styleUrls: ['celebrities-tab.page.scss']
})
export class CelebritiesTabPage {

  constructor(private modalController: ModalController) {}

  public async openPromoModal(): Promise<any> {
    const modal = await this.modalController.create({
      component: PromoModalComponent,
      showBackdrop: true,
      cssClass: 'promo-modal'
    });
    modal.present();
  }

}
