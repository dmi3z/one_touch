import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Tabs } from './constants/tabs.enum';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {

  public activeTab: Tabs;
  public tabs = Tabs;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {}

  public ngOnInit(): void {
    this.activatedRoute.url.subscribe(() => {
      const isTestActive = this.router.url.includes('test-view') || this.router.url.includes('celebrities') || this.router.url.includes('friends');
      if (isTestActive) {
        document.body.className = 'inverse';
      } else {
        document.body.className = '';
      }
    });
  }

  public onTabChange({ tab }) {
    this.activeTab = tab;
  }

}
