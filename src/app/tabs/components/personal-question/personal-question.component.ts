import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PersonalQuestion } from 'src/app/types/questions.interface';

@Component({
    selector: 'app-personal-question',
    templateUrl: 'personal-question.component.html',
    styleUrls: ['personal-question.component.scss']
})

export class PersonalQuestionComponent {
    @Input() public personalQuestion: PersonalQuestion;
    @Input() selectedValue: number;

    @Output() selectOption = new EventEmitter<any>();

    constructor() { }

    public onOptionChange(): void {
        this.selectOption.next(this.selectedValue);
    }

}
