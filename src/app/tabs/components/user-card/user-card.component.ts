import { Router } from '@angular/router';
import { Component } from '@angular/core';

@Component({
    selector: 'app-user-card',
    templateUrl: 'user-card.component.html',
    styleUrls: ['user-card.component.scss']
})

export class UserCardComponent {

    constructor(private router: Router) { }

    public openCompare(): void {
        this.router.navigate(['tabs/compare']);
    }
}
