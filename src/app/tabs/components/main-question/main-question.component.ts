import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-main-question',
    templateUrl: 'main-question.component.html',
    styleUrls: ['main-question.component.scss']
})

export class MainQuestionComponent {
    @Input() public mainQuestion: string[];
    @Input() public selectedValue: number;
    @Input() public currentStep: number;
    @Input() public stepLength: number;
    public readonly answersScale = [-2, -1, 0, 1, 2];

    @Output() public selectOption = new EventEmitter<any>();

    public onChange(): void {
        this.selectOption.next(this.selectedValue);
    }
}
