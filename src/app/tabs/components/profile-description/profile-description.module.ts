import { CommonModule } from '@angular/common';
import { NgModule } from "@angular/core";
import { ProfileDescriptionComponent } from "./profile-description.component";

@NgModule({
    declarations:[
        ProfileDescriptionComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ProfileDescriptionComponent
    ]
})

export class ProfileDescriptionModule { }
