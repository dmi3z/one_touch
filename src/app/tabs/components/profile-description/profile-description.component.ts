import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-profile-description',
    templateUrl: 'profile-description.component.html',
    styleUrls: ['profile-description.component.scss']
})

export class ProfileDescriptionComponent {
    @Input() value: string[];
}
