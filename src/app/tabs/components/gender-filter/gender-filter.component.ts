import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Gender } from 'src/app/enums/gender.enum';

@Component({
    selector: 'app-gender-filter',
    templateUrl: 'gender-filter.component.html',
    styleUrls: ['gender-filter.component.scss']
})

export class GenderFilterComponent implements OnInit {
    public selectedValue: Gender;
    public genders = Gender;

    @Output() genderChange = new EventEmitter<Gender>();

    constructor() { }

    public ngOnInit(): void {
        this.selectedValue = Gender.MALE;
    }

    public onGenderChange(): void {
        this.genderChange.next(this.selectedValue);
    }
}
