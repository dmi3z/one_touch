import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GenderFilterComponent } from './gender-filter.component';

@NgModule({
    declarations: [
        GenderFilterComponent
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        GenderFilterComponent
    ]
})

export class GenderFilterModule { }
