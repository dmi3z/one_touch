import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NavigationStep } from '../../constants/navigation.enum';

@Component({
    selector: 'app-navigations',
    templateUrl: 'navigations.component.html',
    styleUrls: ['navigations.component.scss']
})

export class NavigationsComponent {

    @Input() public isNextHidden: boolean;
    @Output() public navigationEvent = new EventEmitter<NavigationStep>();

    constructor() {}

    public prevStep(): void {
        this.navigationEvent.next(NavigationStep.PREVIOUS_STEP);
    }

    public nextStep(): void {
        this.navigationEvent.next(NavigationStep.NEXT_STEP);
    }
}
