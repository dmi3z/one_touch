import { CommonModule } from '@angular/common';
import { NavigationsComponent } from './navigations.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [
        NavigationsComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        NavigationsComponent
    ]
})

export class NavigationsModule { }
